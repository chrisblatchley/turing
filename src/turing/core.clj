(ns turing.core)

(defn create-tape
  []
  (list [0] [0]))

(defn create-machine
  [state tape rules]
  {:state state :tape tape :rules rules})

(defn head-left
  [[left right]]
  (let [top (peek left)
		new-left (pop left)
		next-left (if (empty? new-left) '(0) new-left)
		next-right (vec (cons top right))]
	(list next-left next-right)))

(defn head-right
  [[left right]]
  (let [top (peek right)
		tmp-right (pop right)
		next-right (if (empty? tmp-right) '(0) tmp-right)
		next-left (vec (cons top left))]
	(list next-left next-right)))

(defn read-head
  [[left right]]
  (peek left))

(defn write-head
  [v [left right]]
  (let [popped-left (pop left)
		next-left (vec (cons v popped-left))]
	(list next-left right)))

(defn add-rule
  [state in out dir next rules]
  (let [rule {:out (partial write-head out)
			  :dir (if (= dir :left) head-left head-right)
			  :next next}]
	(if (state rules)
	  (assoc-in rules [state in] rule)
	  (assoc rules state {in rule}))))

(defn compile-rules
  [rules]
  (letfn [(reduce-rule
		  [rules-partial [state in out dir next]]
		  (add-rule state in out dir next rules-partial))]
	(reduce reduce-rule {} rules)))

(defn do-step
  [input {:keys [rules state tape]}]
  (let [rule (get-in rules [state input])
		in (read-head tape)
		out (:out rule)
		next-state (:next rule)
		]
	(create-machine
	 next-state
	 ((:dir rule) (write-head out tape))
	 rules)))

(defn run-simulation
  [machine input])

(def rules (compile-rules [[:a :0 :0 :left :a] [:a :1 :1 :right :a]]))

(def tm (create-machine :a (create-tape) rules))

tm

(do-step :1 tm)










